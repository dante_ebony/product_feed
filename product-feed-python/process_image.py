import logging
import tempfile
import requests
from PIL import Image
#from scrapy.conf import settings

raw_img_path = ""
proc_img_path = ""


class ProcessImage(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def get23ratio(self, size, isheight):
        width = height = 0
        if (isheight):
            height = size 
            width = size * 0.66666666666666666
        else:
            width = size
            height = size * 1.5

        return (int(width), int(height))

    def downloadEditAndUploadImage(self, item):

        print ('image post start')

        API_ADD_IMAGE='images/imageupload'
        BASE_API = 'https://api.asaan.com/asaanv14/'


        im = Image.open(requests.get(item['image'], stream=True).raw)

        curImageSize = im.size

        if (max(curImageSize[0], curImageSize[1]) > 3000):

            im.thumbnail((3000,3000), Image.ANTIALIAS)

            curImageSize = im.size

        # now get the max dimension and get ratios accordingly
        if (curImageSize[0] >= curImageSize[1]):
            newImageSize = self.get23ratio(curImageSize[0], False)
        else:
            newImageSize = self.get23ratio(curImageSize[1], True)

        imgContainer = Image.new('RGB', newImageSize, "white")

        topMargin = int((newImageSize[1] - curImageSize[1]) / 2)

        leftMargin = int((newImageSize[0] - curImageSize[0]) / 2)

        imgContainer.paste(im, (leftMargin, topMargin))

        requestData = {"imageId": item.get('productId')}

        erroredImageCount = 0

        with tempfile.TemporaryFile("w+b") as file:

            imgContainer.save(file, format=im.format)
            file.seek(0)
            response = requests.post(BASE_API + API_ADD_IMAGE,
                                     files={'image': ("image" + "_" + str(0), file, 'image/jpeg')},
                                     data=requestData)
            print (response.json())
            if not response.ok:
                erroredImageCount += 1
                # buf = BytesIO()
                # imgContainer.save(buf,format=im.format)
                # buf.seek(0)
        return erroredImageCount

