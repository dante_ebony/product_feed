import pymongo
from post_product import Postproduct
from process_image import ProcessImage
from utility_helper import UtilityHelper


class Feed():

    name = 'asos'
    country = 'US'

    def __init__(self):
        
        self.feed = ''
        self.batch = 500

        client = pymongo.MongoClient('mongodb://34.237.83.126:27000/')
        db = client['feed']
        feed_data = db[self.__class__.name]
        self.feed = feed_data



    def start(self):

       no_of_item =  self.feed.find({'error':{ '$exists': False }}).count()

       print(no_of_item)

       if no_of_item > 0:

        no_of_batch = (no_of_item / self.batch)

        print(no_of_batch)

        for i in range(0,no_of_batch + 1):

            print(i*self.batch)

            result = list(self.feed.aggregate([
                            {'$match': {'error':{ '$exists': False }}},
                            {'$skip': i*self.batch},
                            {'$limit': 500},
                            {'$group': {'_id': '$name', 'products': {'$push': "$$ROOT"}}},
                            {'$project': {'product': {'$arrayElemAt': ["$products", 0]}}},

                       ]))

            print result
            for item in result:


                asaancat = UtilityHelper().getCat(item['product']['categoryText'], self.__class__.name)

                if asaancat:

                    data = UtilityHelper().getPostData(item, asaancat, Feed.country)
                    res = Postproduct().process_item(data)

                    #if not res.get('error') == True:

                        #ProcessImage().downloadEditAndUploadImage(res.get('item'))

                   #






        


if __name__ ==  '__main__':

    f = Feed()
    f.start()




