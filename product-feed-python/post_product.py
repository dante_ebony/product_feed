import json
import requests
import logging
import pymongo


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class Postproduct():

        name = 'asos'

        def __init__(self):

            self.feed = ''
            self.batch = 500

            client = pymongo.MongoClient('mongodb://34.237.83.126:27000/')
            db = client['feed']
            feed_data = db[self.__class__.name]
            self.feed = feed_data

        def process_item(self, item):


            BASE_API='https://api.asaan.com/asaanv14/'

            API_ADD_PRODUCT = 'products/scrapPost'
        
            logger.debug("[%s] Starting Pipeline" % (__name__))

            #if item.get('errored'):
             # logger.debug("[%s] Product already errored" % (__name__))
             #return item

            logger.debug("[%s] Starting Product Upload" % (__name__))

            keys_to_send_in_request = ['productName', 'price', 'dscntdPrice', 'productDesc', 'brand', 'tags', 'webLink',
                                       'appProdId', 'source', 'scrapFlag',
                                       'userRecommendation', 'userId', 'imgCount', 'storeDetails', 'stackIndex',
                                       'countryISOCode', 'gender', 'categoryId', 'color', 'size', 'type', 'material',
                                       'ageGroup' ,'tags', 'additionalData', 'inStock']

            requestData = {key: item.get(key) for key in keys_to_send_in_request}

            print('print Request data',requestData)



            logger.debug("[%s] Request Data %s" % (__name__,requestData))

            retryCount = 0
            responseSucc = False
            responseData = None

        # while not (responseSucc or retryCount < 3):
            retryCount += 1

            response = requests.post(url='https://api.asaan.com/asaanv14/products/scrapPost', data=json.dumps(requestData),
                                     headers={"Content-Type": "application/json"})
            print ('res', response)


            responseSucc = response.ok
            responseData = response.json()
            print('resok',responseSucc)
            print('resdata',responseData)
          # if responseSucc: break

            logger.debug("[%s] Request %s, Response Data %s" % (__name__,requestData,responseData))

            if not responseSucc:

               if responseData.get('prodStatus') and responseData.get('productId'):
                  print('Product already posted and is complete')

                  self.feed.update_one({'merchantProductId': item.get('appProdId')},
                                       {
                                           '$set': {
                                               'error': True,
                                               'reason': responseData
                                           }
                                       }
                                       )

                  return {
                      'error': True,
                      'msg': 'Product already posted and is complete',
                      'productId': responseData.get('productId')
                  }

               elif responseData.get('productId'):
                   print("[%s] Product already posted and but not complete" % (__name__))

                   self.feed.update_one({'merchantProductId': item.get('appProdId')},
                                        {
                                            '$set': {
                                                'error': True,
                                                'reason': responseData
                                            }
                                        }
                                        )

                   return {
                       'error': True,
                       'msg':'Product already posted and but not complete',
                       'productId': responseData.get('productId')
                   }

               else:
                  print("[%s] Some error occured while posting product" , responseData)

                  self.feed.update_one({'merchantProductId': item.get('appProdId')},
                                       {
                                           '$set': {
                                               'error': True,
                                               'reason': responseData
                                           }
                                       }
                                       )

                  return {
                      'error': True,
                      'msg': responseData,
                  }



            else:
                  print("[%s] Product successfully posted",responseData.get('productId'))
                  item['productId'] = responseData.get('productId')

                  self.feed.update_one({'merchantProductId': item.get('appProdId')},
                                       {
                                           '$set': {
                                               'error': False,
                                               'reason': responseData
                                           }
                                       }
                                       )
                  return {
                      'error': False,
                      'productId': responseData.get('productId'),
                      'item':item
                  }

