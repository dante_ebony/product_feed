from pymongo import MongoClient
import urllib2
import re


class UtilityHelper():

    def __init__(self):

        client = MongoClient('mongodb://34.237.83.126:27000/')
        self.db = client['feed']

    def getCat(self, catText, store):

        catMap = self.db['categoryMap']

        result = catMap.find_one({'category': re.compile(catText, re.IGNORECASE), 'store': store}, {'_id': False})

        if result is not None:
            return result
        else:
            return False

    def getUser(self, country):

        user = self.db['user']

        userObj = list(user.aggregate([
            {
                '$match': {
                    'countryISOCode': country,
                }
            },

            {
                '$sample': {
                    'size': 1,
                }
            }
        ]))

        return userObj[0].get('userId')

    def parseUrl(self, url):

        regex = re.compile('(?<=murl=)(.*)(?=\?)')
        r = regex.search(urllib2.unquote(url))

        return r.group()


    def getPostData(self, item, asaancat, country):

        obj = {}

        if item['product']['desc'] :
            obj['productDesc'] = item['product']['desc']

        if item['product']['discountedPrice']:
            obj['dscntdPrice'] = item['product']['discountedPrice']

        # if item['product']['color']:
        #     obj['color'] = item['product']['color']

        if item['product']['size']:
            obj['size'] = [item['product']['size']]

        obj['categoryId'] = asaancat['asaanCat']

        obj['price'] = item['product']['price']

        obj['brand'] = item['product']['brand']

        obj['gender'] = item['product']['gender']

        obj['productName'] = item['product']['name']

        obj['userId'] = UtilityHelper().getUser(country)

        obj['image'] = item['product']['image']

        obj['countryISOCode'] = country

        obj['scrapFlag'] = True

        obj['inStock'] = True if item['product']['availability'] == 'in-stock' else False

        obj['appProdId'] = item['product']['merchantProductId']

        obj['webLink'] = self.parseUrl(item['product']['url'])

        return obj
